import logo from './logo.svg';
import './App.css';
import {React,useState} from 'react';
import ReactDom from 'react-dom';
import "bootstrap/dist/css/bootstrap.css";
import Beranda from "./components/Beranda";
import Navbar from './assets/Navbar';
import {BrowserRouter,Routes,Route} from "react-router-dom";
import ManajemenBuku from "./components/ManajemenBuku";
//const [books, setBooks] = useState()
function App() {
  const [books, setBooks] =useState([
    {id :1, judul :"Laskar Pelangi", pengrang:"andrea", harga:10000, stok:10 },
    {id :2, judul :"Bumi", pengrang:"Tere", harga:20000, stok:6 },
  ])
  return (
    <div>
      <BrowserRouter>
      <Navbar />
      <Routes>
        <Route exact path ="/" element={ <Beranda />}/>
        <Route exact path ="/ManajemenBuku" element={<ManajemenBuku bookList ={books}/>}/>
      </Routes>
      </BrowserRouter>
    </div> 
  );
}

export default App;
