import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
function ManajemenBuku ({bookList}){
    console.log(bookList)
    return (
        <div id = "daftarBuku">
        <h2 className = "mt-3">DaftarBuku</h2>
        <button className = "btn btn-primary m-2">Tambah Buku</button>
        <table className = "table table-bordered">
            <thread>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Pengarang</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Aksi</th>
                </tr>
            </thread>
            <tbody>
                
            </tbody>
        </table>
        </div>
    );
}
export default ManajemenBuku